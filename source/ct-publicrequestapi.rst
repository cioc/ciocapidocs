=================================
Client Tracker Public Request API
=================================

Introduction and Overview
*************************

The CIOC Client Tracker has support for accepting requests from the public. Client Tracker administrators may designate a set of Surveys as publicly available when used with a specific Classification of Inquiry. Individual survey question parts must also be flagged as public in order for that question/part of a survey to be available on public forms. Requests submitted by members of the public will come in similar to "feedback" in the Online Resources software. These requests go into a queue, where they must be accepted and processed by a staff member or - if they are not appropriate - the requests can be rejected. No public request is included in any other search or report until it is first accepted by a staff member.

There are three methods for generating and processing a public request form:

* **Directly in Client Tracker** (best for those with no website or no technical expertise)
* **Partially-embedded in a remote site** (best for those with a website and minimal technical expertise)
* **Fully-embedded in a remote site** (best for those who want their visitors to always remain in their website during the request process)

All use of the API is provided through HTTP requests to URLs of the form ``https://clienttracker.cioc.ca/public/[AGENCY_CODE]/[CLASSIFICATION_CODE]`` where ``[AGENCY_CODE]`` is the 3 letter agency code of staff members that will handle the public request and ``[CLASSIFICATION_CODE]`` is a code from a Classification of Inquiry.

API Methods
***********

Method 1: Using Forms Created Directly in the Client Tracker
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

With the method 1, the public user navigates with a browser to a URL as indicated above. No further programming is required; the software takes care of generating and processing the form. To use this method, it would be recommended to provide a simple link to the address ``https://clienttracker.cioc.ca/public/[AGENCY_CODE]/[CLASSIFICATION_CODE]`` with substitutions for your Agency Code and the Classification of Inquiry code, as described above.

Method 2 & 3: External Website Integration
++++++++++++++++++++++++++++++++++++++++++

.. warning:: Technical stuff ahead!

In methods 2 and 3, a remote server makes a HTTP ``GET`` request to a URL as indicated above including an ApiKey query string parameter with a valid Public API Key (see next section). The remote server will be provided with a JSON response that includes the HTML rendered form contents (without form tag) and other relevant data as described below in the section covering JSON GET Responses.

In method 2 (partially-embedded in a remote site), the HTML form is configured to POST to the same URL as the GET request and the optional redirect_to query string parameter can be provided indicate a URL to which visitors will be redirected after their request has been successfully submitted. If no redirect_to query string parameter is provided, then the visitor will be redirected to a generic thank you message that can be viewed at ``https://clienttracker.cioc.ca/thanks``.

In method 3 (fully-embedded in a remote site), the same URL used to to handle a ``GET`` request accepts a ``POST`` request with an ``application/x-www-form-urlencoded`` encoded body and returns a JSON encoded response as described in below in JSON POST Responses. As with the remote GET request, a valid ``ApiKey`` parameter is required. The ApiKey parameter may be provided as part of the query string or in the POST body.

Public API Keys
```````````````

A super user may create API keys for their Client Tracker membership in the "Public Request API Keys" section of the Client Tracker Administration area. Keys are Base 64 encode random strings that look something like `bJITwVg9GvEBgW7nDI0N3bZF6U5hUGpaHN2kqQjS6jTg`. A name may be associated with the Key to remind administrators of the intended use of the key, but this is not used in the validation process. Because keys are base 64 encoded they may contain special characters (like +) which require escaping before use in a query string or ``application/x-www-form-urlencoded`` encoded ``POST`` body.

.. warning:: The API key is not intended to be exposed on public pages.

JSON Get Response
`````````````````

When using method 2 or 3 (partially- or fully-embedded in a remote site) the current form is requested from the Client Tracker application with a get request to ``https://clienttracker.cioc.ca/public/[AGENCY_CODE]/[CLASSIFICATION_CODE]?ApiKey=[API_KEY]``. The Client Tracker will respond with a JSON encoded response like the following:

.. code-block:: json

	{
		"alert": null, 
		"form": "<!-- HTML Form Content -->",
		"errors: null
	}

Because there were no errors, both alert and errors are null. form normally contains the contents of the form to display, without the ``<form>`` tag. The form tag is omitted so that you can decide to handle the POST back from the visitor's browser or have the Client Tracker site handle it for you.

JSON POST Response
``````````````````

When using mode 3 (fully-embedded in remote site) the visitor's form submission is is handled by the remote site and a POST request is made on the visitor's behalf to the Client Tracker with the request data. If there is no error, the following JSON encoded response will be returned:

.. code-block:: json

	{
		"success": true
	}

In the event of a validation error the Client Tracker will return a JSON encoded response like the following:

.. code-block:: json

	{
		"alert": "General Error Message", 
		"form": "<!-- HTML Form Content -->",
		"errors: {
			"FieldName": "Specific Error",
			"FieldName2": "Other Specific Error"
		}
	}

As with the JSON Get Response the full html for the HTML form is returned as form. alert will always contain an error message that should be displayed to the visitor that explains the errors in a general way. If there were basic form value validation errors, errors will also contain the specific fields that had an error and details of the error. Note that these messages have already been rendered into the form HTML content but may be useful if you perform an AJAX POST to your site and want to return the error messages back to the page for visitor notification.
