===========================
Client Tracker Resource API
===========================

Overview
********

The Client Tracker allows for interaction with Resource Providers through a simple XML-based protocol. Provisions are made for registering a Resource Provider (aka Resource Type), fetching Resource Item details, determining that a user is currently handling a Request, and pushing Resource Items from a Resource Type into a user's current Request.

The Resource Type must expose URLs to the public Internet to provide Registration and Fetching. Resource Item Pushing requires the Resource Type to connect over the public internet to the Client Tracker host servers, but should still allow interaction with Resource Types that are only accessible via the end user's intranet.

Basic Conventions
+++++++++++++++++

All response messages can contain either a successful response or an error response. The error response can provide a message that will be displayed to the user indicating the source of the error.

Message Types
*************

Registration
++++++++++++

The registration message is designed to simplify the configuration of a Resource Type in the Client Tracker system. While all configuration items can be entered by hand, the registration message ensures accuracy when setting complex URL settings.

The Client Tracker server initiates a request for registration data by performing a HTTP GET request to the Resource Type's registration URL. The definition of the Registration response is defined in :download:`registration.xsd <../schemas/registration.xsd>`. The URL is stored with the other settings so that changes to the registration information can be fetched at the administrator's request.

Resource Item Fetching
++++++++++++++++++++++

The Client Tracker allows the option of having a user fetch Resource Item details from the Resource Type by ID. At the user's request the Client Tracker server performs an HTTP GET to the configured Fetch URL. The ID value entered by the user is substituted for any instance of ``[ID]`` in the Fetch URL. The definition of the Fetch response is defined in :download:`resourceinfo.xsd <../schemas/resourceinfo.xsd>`. The ID may be of any type that can be stored in 50 Unicode characters or less.

Resource Item Pushing
+++++++++++++++++++++

The Client Tracker also allows the Resource Type to push Resource Items into the user's current Request. There are 3 pieces of information that the Client Tracker provides to the Resource Type that must be provided with every request to the Client Tracker:

#. The name of the user for which the request is being made
#. The remote key
#. The Client Tracker's internal id for the Resource Type

These pieces of information are provided to the Resource Type for the current user through the Launch URL. When the user "Launches" a Resource Type, the Client Tracker opens a new browser window directed at the Launch URL where any instance of ``[CTID]``, ``[LOGIN]`` and ``[KEY]`` are replaced with the appropriate values to be provided in messages from the Resource Type. These values could be stored in the user's session with the Resource Type.

The Resource Type can determine if the user is currently in a Request by making an HTTP POST request to ``https://clienttracker.cioc.ca/ct/rpc/is_in_request`` where the body of the POST is an isInRequest message as defined by :download:`incallrequest.xsd <../schemas/incallrequest.xsd>`. The client tracker will respond with an isInRequest response message as defined in :download:`incallresponse.xsd <../schemas/incallresponse.xsd>`. This response will include a list of Resource Item ids that have already been associated with the user's current Request, if applicable.

The Resource Type can push a Resource Item into the user's current Request by making an HTTP POST request to ``https://clienttracker.cioc.ca/ct/rpc/add_resource`` where the body of the POST is a pushResource message as defined in :download:`pushresource.xsd <../schemas/pushresource.xsd>`, with a response message as defined in :download:`pushresourceresponse.xsd <../schemas/pushresourceresponse.xsd>`.
