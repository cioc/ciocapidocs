======================================
CIOC Online Resources API Introduction
======================================

Available API Types
*******************

CIOC has several APIs written specific members, an AIRS XML Export API, and one general use API for searching and record details that is available for both the Community Information and Volunteer modules.

The APIs available as of the last update to this documentation are:

* :doc:`or-stdsearchapi`
* :doc:`or-airsexport` [*]_
* :doc:`or-shareexcelexport`
* 211ontario.ca Export ``Custom API``
* O211 Export ``Custom API, DEPRECATED``
* CLBC Export & CLBC Update ``Custom API``

To search and/or display records from an external website, the correct API to use is the :doc:`or-stdsearchapi`. In addition to formal APIs, it is possible to create a :doc:`or-shareexcelexport`, which may be useful in limited circumstances to automate regular download of an custom-formatted Excel/CSV file for use in an external system.

.. note:: This document **does not cover use of the 211ontario.ca, O211 Export, or CLBC Export / Update APIs**. These are purpose-built for specific external systems, and not generally applicable for use by others.

.. [*] Although the AIRS Export was purpose-built for a particular organization and has some modifications from the standard, it may still be suitable for others, particularly those sharing to iCarol.

API Security
************

All APIs require user authentication. It is highly recommended that SSL and a compatible domain (i.e. \*.cioc.ca) be used; support of non-SSL domains is deprecated and will not be supported in the future. Authentication uses HTTP Basic Authentication. It is recommended that API Accounts be be configured on a **per-project basis**, and used **exclusively for the API** for the given project. Non-API (regular user) accounts should generally not be given API access. Because of the nature of how API account information is accessed and stored, these accounts should never be provided with any update or setup privileges of any kind. If you have a developer that needs these extra privileges, they should have one account for themselves and a different account for production API use.

.. warning:: **Do not under any circumstances include account credentials in public scripts or webpages or distribute them in clear-text with applications**

Each API requires a user account with permission to access that specific API. By default, no user has access to any of the APIs. All APIs except the Excel/CSV Feed are enabled in the User Type Configuration in the API Section. Because the CSV/Excel export tool has a front-end UI for users as well, any account that can access a particular Excel Output Profile through the regular UI can also access the feed; view the documentation for :doc:`or-shareexcelexport` for more information on access control for Excel Output Profiles.

.. note:: The User Type and assigned View determine which records can be accessed through the API [*]_. Records available in each API are the same as the account would have access to if used through the regular software UI, so logging into the UI with the API account is the best method to confirm that access restrictions are correct.

Most purpose-built APIs have a **fixed set of fields** that cannot be changed, while CIOC's :doc:`or-stdsearchapi` has customizable data access restrictions that mirror what a User would see if logged in to the regular UI. Do not allow the API account to have access to Views that should not be displayed via the API (e.g. via View switching). Best practice often means configuring a View for the API on a per-project basis so that the security and data access can be properly controlled.

.. warning:: In general, an API View should not be configured to have switching access to other Views unless that is intended functionality based on how you are choosing to use the API.

.. [*] The CLBC Export & Update APIs and the deprecated O211 Export API have fixed criteria and do not used View-based criteria. Usage of these APIs are not covered by this documentation.

API Statistics
**************

"Record View" statistics are kept for the :doc:`or-stdsearchapi`. There is an API flag in the Record View statistics table in the software to indicate when the Record View was the result of an API call, and the User ID of the account calling the API is also recorded. This information can be used to do reporting on the use of the API for a specific account/project. Note that API calls register as "logged in" uses for statistical purposes.