======================================
Standard Remote Search and Details API
======================================

.. note:: For general information on CIOC Online Resources APIs, including information on configuring user accounts to have API access, read the :doc:`or-intro`.

Introduction
************

This *Standard Remote Search and Details API* is designed to be a flexible, general-purpose solution to embedding search results and record details on another (non-CIOC) site. It is intended to be used in real-time, in that a request to the remote site results in a request to this API. All API endpoints are avialable in **JSON** (JavaScript Object Notation), a lightweight data interchange format that is commonly used to exchange data over the internet. The major endpoints are also available as **XML**.

All API endpoints except the *Volunteer Application* API (aka *"Yes, I'd Like to Volunteer!"*) accept ``GET`` requests and take parameters as a URL-encoded query string. The *Volunteer Application* endpoint takes a ``POST`` request with a ``application/x-www-form-urlencoded`` body. There is one common parameter that all endpoints accept: ``format``, which may be set to ``xml`` to override the default return format of JSON and instead return an XML formatted result. 

Search API
**********

The Search API is useful for producing a list of records according to a specific criteria. The fields available are configurable (See :ref:`search-config-fields`). The Search API is intended to be used "live" to search the database from an external system, though the implementer may choose to provide some periodic caching of search results to reduce transit for common requests, such as generating a list of records for a webpage that lists services in a particular category. If caching results, a reasonable refresh period should be determined.

.. _search-config-fields:

Configuring Fields
++++++++++++++++++

The API results must include either Record Number *or* Name, and the link to the Details API for the given record. Many additional items can be added to the results through the setup for the View connected to the API Account.

.. note:: It is strongly recommended that the number of fields be limited to the record name plus 2 or 3 additional fields; because a very large number of records can be returned, the inclusion of	many fields can create enormous request sizes.

The list of fields is configured through the **Display Options of the API User Account's View** in the **Search Results** section (see image below - Community Information settings shown). The *Show Options* and *Print Version* settings do not apply to the API. To enable the Latitude and Longitude fields in the API, turn on the *Map Search Results with Google Maps* option. Best practice is for each API project to have its own View, which allows control over what the API sees without impacting other uses. 

.. image:: ../images/apidisplayoption.png
   :alt: Community Information View Search Options


Access URL and Search Parameters
++++++++++++++++++++++++++++++++

The main Search API is accessed through the following URLs (substituting your domain):

Community Information : Search
	``https://yoursite.cioc.ca/rpc/orgsearch.asp``

Volunteer Opportunity : Search
	``https://yoursite.cioc.ca/rpc/oppsearch.asp``

Volunteer Opportunity : Browse by Organization
	``https://yoursite.cioc.ca/rpc/browseoppbyorg``

All Search API URLs accept a **format** parameter ``format=[xml|json]``. If no format is specified, the default is JSON.

The Community Information and Volunteer **Search** API URLs support all search parameters available to the API account from the regular Search Results page from the UI (``results.asp``/``bresults.asp`` in the UI), but at this time it does not include Saved Searches which are accessed through ``sresults.asp`` or some of the specialized Child Care search types accessed only through ``cresults.asp``). The Volunteer *Browse by Organization* URL does not accept any additional search parameters, and always returns a list of organizations with active opportunities.

Which Parameters to Use?
````````````````````````
There are hundreds of search parameters in the software, and which ones are available to the current user depend on various setup options. The best way to find the parameters available to the current user is by reviewing the Basic or Advanced Search forms.

Method 1: Execute a Search, Parse the URL
	Execute any search that posts to ``results.asp`` or ``bresults.asp``, and view the available parameters in the URL of the results page. This includes Basic and Advanced Search, all Taxonomy Searches, Volunteer Step-by-Step Search, and any search linked from the Record Details page.

Method 2: Copy the Form
	You can directly copy HTML from the Basic or Advanced Search form and use it in another application.

Method 3: Turn on Search Parameter Key Display
	The Basic and Advanced Search forms have a special settings to allow you to see what the search parameters would be as you type criteria into the form:

.. image:: ../images/searchparamkey.png
   :alt: Example of Search Parameter Key Display

Available search forms with the Search Parameter Key Display:

	* ``https://yoursite.cioc.ca/?SearchParameterKey=on``
	* ``https://yoursite.cioc.ca/advsrch.asp?SearchParameterKey=on``
	* ``https://yoursite.cioc.ca/volunteer/?SearchParameterKey=on``
	* ``https://yoursite.cioc.ca/volunteer/advsrch.asp?SearchParameterKey=on``

.. note:: **It is intentionally difficult to produce a complete list of records with no criteria**. The Search API is *not* intended to be used for complete database downloads: use the :doc:`or-shareexcelexport` for that purpose in the Community Information module. In the Volunteer Opportunities module, a complete list of current opportunities can be retrieved using the criteria ``DisplayStatus=C`` to retrieve all currently active opportunities.

.. note:: **Get Clarity with Codes**: Many fields that appear with numeric IDs when searching in the regular CIOC UI can be converted to use human-readable Codes instead. Using Codes provides clarity, predictability, and portability and is highly recommended. A search with **ID** in it (``PBID``, ``GHID``, ``AIID``, and so on...) can be converted to a Code search by changing ID to Code (``PBCode``, ``GHCode``, ``AICode``, and so on...). You can add/edit the codes for each item and display a list of available codes via the Setup Area for the given item. General Headings can be searched by Code as explained here, but also by *Name* - just change the ``GHType`` parameter to ``NM`` and the ``GHID`` parameter to ``GH``.

Download the Schemas
++++++++++++++++++++

The following schemas indicate what to expect when retrieving output in XML. If using a JSON format, the structure and contents are the same and the XSD serves as a reasonable guide. See the :ref:`sample-search-json` for more examples.

* :download:`Community Information: Search API Schema <../schemas/cicsearchapi.xsd>`
* :download:`Volunteer Opportunity: Search API Schema <../schemas/volsearchapi.xsd>`
* :download:`Volunteer Opportunity: Browse by Organization API Schema <../schemas/volbrowseorgapi.xsd>`

There are 3 pieces of information provided by default in the search results for the Search APIs:

* ``NUM``/``VNUM`` - The Record ID
* ``RECORD_DETAILS`` - A link to the record on the CIOC Site
* ``API_RECORD_DETAILS`` - A link to the API URL for the record

Other fields provided in the results correspond to the Search Results settings in the account's View. A list of possible **Field name values** is available from the current `CIOC Online Resources Field List`_.

Error Handling
++++++++++++++

**JSON** search results will be an object with a string ``error`` property. If there is an error of any kind, the ``error`` property will be a string value explaining the error. If there is no error, the error property will be ``null``.

**XML** search results will have a ``<root>`` element containing an ``<error>`` element and a ``<recordset>`` element. If there is an error of any kind, the ``<error>`` element will contain a string value explaining the error and the <recordset> element will be a self-closed empty element. If there is no error, the <error> element will be a self-closed empty element, and the ``<recordset>`` element will contain a series of ``<record>`` elements (or if no records match the query, the ``<recordset>`` element will also be a self-closed empty element).

.. note:: **Sanitize your inputs**. Although CIOC processes inputs that come via the API, it is *emphatically* recommended that external applications using the API take some responsibility for minimizing potential disruption to the production system that can be caused by bad actors. Perform a basic level of pre-processing of public requests before passing them to the API by only passing on parameters you are actually expecting to receive, and checking the type of those parameters when appropriate. This also allows an extra level of protection if you have unintentionally configured the security of your API to have access to more data than you intended.

Sample XML
++++++++++

Community Information Search Result
```````````````````````````````````

.. code-block:: xml

	<root>
		<error/>
		<recordset>
			<record NUM="ABC00001">
				<field name="RECORD_DETAILS">https://yoursite.cioc.ca/record/ABC00001</field>
				<field name="API_RECORD_DETAILS">https://yoursite.cioc.ca/rpc/record/ABC00001?format=xml</field>
				<field name="ORG_NAME">Organization Name</field>
				<field name="LATITUDE">43.718193</field>
				<field name="LONGITUDE">-79.722915</field>
				<field name="LOCATED_IN_CM">Brampton (City of)</field>
			</record>
		</recordset>
	</root>

Volunteer Opportunity Search Result
```````````````````````````````````

.. code-block:: xml

	<?xml version="1.0" encoding="utf-8"?>
	<root>
		<error/>
		<recordset>
			<record VNUM="V-DEF0001" OPID="123">
				<field name="RECORD_DETAILS">https://yoursite.cioc.ca/volunteer/record/V-DEF0001</field>
				<field name="API_RECORD_DETAILS">http://tika-dev/rpc/opportunity/V-DEF0001?format=xml</field>
				<field name="POSITION_TITLE">Position Title</field>
				<field name="ORG_NAME">Organization Name</field>
				<field name="COMM_BALLS">&lt;img src=&quot;https://yoursite.cioc.ca/images/teal_ball.gif&quot; alt=&quot;Community 1&quot; title=&quot;Community 1&quot;&gt;</field>
				<field name="DUTIES">Description of Duties</field>
				<field name="INTERESTS">Interest 1; Interest 2; Interest 3</field>
			</record>
		</recordset>
	</root>

Volunteer Opportunity: Browse by Organization
`````````````````````````````````````````````

.. code-block:: xml

	<?xml version="1.0" encoding="utf-8"?>
	<root>
		<item>
			<ORG_NAME>Organization Name</ORG_NAME>
			<NUM>ABC00001</NUM>
			<OP_COUNT>4</OP_COUNT>
			<OPP_SEARCH_LINK>https://yoursite.cioc.ca/rpc/oppsearch.asp?NUM=ABC00001&amp;format=xml</OPP_SEARCH_LINK>
			<API_RECORD_DETAILS>https://yoursite.cioc.ca/rpc/record/ABC00001?format=xml</API_RECORD_DETAILS>
		</item>
	</root>

.. _sample-search-json:

Sample JSON
+++++++++++

Community Information Search Result
```````````````````````````````````

.. code-block:: json

	{
		"error":null,
		"recordset":[
			{
			"NUM":"ABC00001",
			"RECORD_DETAILS":"https://yoursite.cioc.ca/record/ABC00001",
			"API_RECORD_DETAILS":"https://yoursite.cioc.ca/record/ABC00001",
			"ORG_NAME":"Organization Name",
			"LATITUDE":43.718193,
			"LONGITUDE":-79.722915,
			"LOCATED_IN_CM":"Brampton (City of)"
		   }
		]
	}

Volunteer Opportunity Search Result
```````````````````````````````````

.. code-block:: json

	{
		"error":null,
		"recordset":[
			{
			"OPID":123,
			"VNUM":"V-DEF0001",
			"RECORD_DETAILS":"https://yoursite.cioc.ca/record/DEF0001",
			"API_RECORD_DETAILS":"https://yoursite.cioc.ca/record/DEF0001",
			"POSITION_TITLE":"Position Title",
			"ORG_NAME":"Organization Name",
			"COMM_BALLS": "<img src=\"https://yoursite.cioc.ca/images/teal_ball.gif\" alt=\"Community 1\" title=\"Community 1\">",
			"DUTIES":"Description of Duties",
			"INTERESTS":"Interest 1; Interest 2; Interest 3"
		   }
		]
	}

Volunteer Opportunity: Browse by Organization
`````````````````````````````````````````````

.. code-block:: json

	[
		{
			"ORG_NAME": "Organization Name",
			"NUM": "ABC00001",
			"OP_COUNT": 4,
			"OPP_SEARCH_LINK": "http://yoursite.cioc.ca/rpc/oppsearch.asp?NUM=ABC00001",
			"API_RECORD_DETAILS": "http://tika-dev/rpc/record/ABC00001"
		}
	]

Additional Feeds
++++++++++++++++

Using Supplementary JSON Feeds
``````````````````````````````

To assist with performing searches, there are JSON-only feeds available that list available search criteria in some categories. All feeds are in the location: ``https://yoursite.cioc.ca/jsonfeeds/``

Communities Feed
````````````````

For use in the **CMID** Parameter:
``community_generator.asp?term=partial+word_string``

Keyword Suggestions (Auto-complete)
```````````````````````````````````
For use in the **STerm** Parameter:

Community Information
	* ``cic_keyword_generator?term=partial+word_string&SearchType=[A|O|T|S]``
	* A = Anywhere (Default), O = Org. Names, T= Taxonomy, S = Subjects
Volunteer Opportunities
	* ``vol_keyword_generator?term=partial+word_string&SearchType=[P|O]``
	* P = Position Keywords, O = Org. Names

Areas of Interest Feed
``````````````````````

For use in the **AIID** Parameter:

All Interest Categories:
	``interest_generator.asp?ShowAll=on``
Keyword Generation:
	``interest_generator.asp?term=partial+word+string``
By Interest Group ID(s):
	``interest_generator.asp?IGID=3,10,22``

General Heading HTML Option List Feed
`````````````````````````````````````

There is a feed for generating **form HTML** for the General Headings of a selected publication. It is output as a list of HTML ``option`` elements. This feed includes **all** Headings, including non-public.

``heading_searchform.asp?PBID=123``

Record Details API
******************

The Record Details API is for pulling the full details of a specific record. It is accessed through the following URLs

Community Information Record:
	``https://yoursite.cioc.ca/rpc/record/[NUM]``

Volunteer Opportunity Record:
	``https://yoursite.cioc.ca/rpc/opportunity/[VNUM]``

Configuring Fields
++++++++++++++++++

The fields available in the Record Details API are identical to what is available when the API account is viewing the Record Details page through the regular UI. To change the fields available or their re-organize them, change the **Field Groups** and **Details Fields** setup of the View used by the API Account. Best practice is for each API project to have its own View, which allows control over what the API sees without impacting other uses.

.. note:: The display order of fields *within* each Field Group is determined by the global Field Order setup. It is not possible to change the global Field Order without impacting every View in the database. Use Field Groups to cluster similar fields together.

Download the Schemas
++++++++++++++++++++

The following schemas indicate what to expect when retrieving output in XML

* :download:`Community Information Details API Schema <../schemas/cicdetailsapi.xsd>`
* :download:`Volunteer Opportunity Details API Schema <../schemas/voldetailsapi.xsd>`

If using a JSON format, the structure and contents are the same and the XSD serves as a reasonable guide.

**Field Group name values** are custom and determined by the View Setup. A list of possible **Field name values** is available from the current `CIOC Online Resources Field List`_.

Error Handling
++++++++++++++

Any errors will be returned as HTTP errors. The only errors supported at this time are 404 (Not Found) and 401 (Permission Denied). 401 errors are provided with a textual reason if appropriate.

Sample JSON
+++++++++++

.. todo:: Coming Soon

Sample XML
++++++++++

.. todo:: Coming Soon

Volunteer Application API
*************************

The *Volunteer Application* API (aka *"Yes, I'd Like to Volunteer!"*)  takes a ``POST`` requests with a ``application/x-www-form-urlencoded`` body to the address ``https://yoursite.cioc.ca/volunteer/volunteer2.asp``. Available parameters can be found on the form at ``https://yoursite.cioc.ca/volunteer/volunteer.asp?VNUM=[ANY_VNUM]&SearchParameterKey=on``. In addition to the parameters in that form, the API request needs to include the parameter ``api=on`` which indicates that the request is an API request and ``VNUM=[vnum]`` which indicates the opportunity to volunteer for.

 .. note:: The API expects a response to CIOC's captcha. **If directly passing on submissions from the public without vetting (e.g. not requiring a user account of some kind or forcing a response to the CIOC captcha) then it is strongly recommended that you provide an alternative captcha**. If you provide an alternative captcha, the API consumer application will still have to respond to the CIOC captcha.
 
.. _CIOC Online Resources Field List: https://kclsolutions.desk.com/customer/portal/articles/2108436
